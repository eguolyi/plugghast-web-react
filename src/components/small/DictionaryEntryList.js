import {Component} from "react";
import PropTypes from "prop-types";
import {drawerWidth} from "../ResponsiveDrawer";
import NestedList from "./NestedList";
import React from "react";
import {withStyles} from "@material-ui/core";

const WORD_CLASS_NOUN_ID = 'wordClassNoun'
const WORD_CLASS_VERB_ID = 'wordClassVerb'
const WORD_CLASS_ADJ_ID = 'wordClassAdj'
const WORD_CLASS_INTERJ_ID = 'wordClassInterj'
const WORD_CLASS_PRON_ID = 'wordClassPron'
const WORD_CLASS_ADV_ID = 'wordClassAdv'
const WORD_CLASS_CONJ_ID = 'wordClassConj'
const WORD_CLASS_PREP_ID = 'wordClassPrep'
const WORD_CLASS_NUM_ID = 'wordClassNum'

const styles = theme => ({
    nested: {
        flex: 1,
        marginTop: 10,
        [theme.breakpoints.down('sm')]: {
            marginRight: 0,
            marginLeft: 0
        },
        [theme.breakpoints.up('sm')]: {
            marginRight: 10,
            marginLeft: drawerWidth
        },
    }
});

class DictionaryEntryList extends Component {
    static propTypes = {
        classes: PropTypes.object.isRequired,
        details: PropTypes.object.isRequired,   // folk lexikon lookup details
        openNativeDialog: PropTypes.func.isRequired,
        dispatch: PropTypes.func.isRequired,
        nativeLanguage: PropTypes.string.isRequired,
        openAlertDialog: PropTypes.func,
        openSaolInflectionDialog: PropTypes.func,
        setCurrentEntryText: PropTypes.func,
        i18nStrings: PropTypes.object.isRequired,
        onOpenSelectNativeDialog: PropTypes.func.isRequired
    }

    render() {
        return (
            <div className={this.props.classes.nested}>
                {
                    getWordsFromXMLResponse(this.props.details).map((xmlWord, i) =>{
                        let entryText = xmlWord.getAttribute("value")
                        let entryClass = xmlWord.getAttribute("class")
                        if (entryClass === null) {
                            entryClass = ''
                        }

                        return (<NestedList key={i}
                                    headString={getOneEntryFromOneWord(this.props.i18nStrings, entryText, entryClass,xmlWord)}
                                    entryLang={xmlWord.getAttribute("lang")}
                                    entryText={entryText}
                                    entryClass={entryClass}
                                    wordDetails={getWordDetailsFromWordNode(xmlWord)}
                                    openNativeDialog={this.props.openNativeDialog}
                                    dispatch={this.props.dispatch}
                                    nativeLanguage={this.props.nativeLanguage}
                                    openAlertDialog={this.props.openAlertDialog}
                                    openSaolInflectionDialog={this.props.openSaolInflectionDialog}
                                    setCurrentEntryText={this.props.setCurrentEntryText}
                                    i18nStrings={this.props.i18nStrings['NestedList']}
                                    onOpenSelectNativeDialog={this.props.onOpenSelectNativeDialog}
                        />)})
                }
            </div>
        )
    }
}

function getWordsFromXMLResponse(xmlResponse) {
    if (xmlResponse instanceof Document) {
        let words = xmlResponse.getElementsByTagName("word")
        let arrWords = [...words]
        return arrWords
    } else {
        return []
    }
}

function getOneEntryFromOneWord(i18nStrings, entryText, entryClass, xmlWord) {

    if (entryClass !== null && entryClass !== '') {
        entryText = entryText + ' ' + getEntryTypeFullName(i18nStrings,entryClass)
    }
    let comment =  xmlWord.getAttribute("comment")
    if (comment !== null && comment !== '') {
        entryText = entryText + ' (' + comment + ')'
    }
    return entryText
}

function getEntryTypeFullName(i18nStrings, shortName) {
    if (shortName === "nn")
        return i18nStrings[WORD_CLASS_NOUN_ID]

    if (shortName === "vb")
        return i18nStrings[WORD_CLASS_VERB_ID]

    if (shortName === "jj")
        return i18nStrings[WORD_CLASS_ADJ_ID]

    if (shortName === "in")
        return i18nStrings[WORD_CLASS_INTERJ_ID]

    if (shortName === "pn")
        return i18nStrings[WORD_CLASS_PRON_ID]

    if (shortName === "ab")
        return i18nStrings[WORD_CLASS_ADV_ID]

    if (shortName === "kn")
        return i18nStrings[WORD_CLASS_CONJ_ID]

    if (shortName === "pp")
        return i18nStrings[WORD_CLASS_PREP_ID]

    if (shortName === "rg")
        return i18nStrings[WORD_CLASS_NUM_ID]

    return ""
}

function getWordDetailsFromWordNode(xmlWord) {

    let xmlChildNodes = [...xmlWord.children]

    let wordDetails = {}
    xmlChildNodes.forEach(xmlChildNode => {
        const tagName = xmlChildNode.tagName

        switch (tagName) {
            case 'translation':
            case 'synonym':
            case 'definition':
            case 'explanation':
                if (wordDetails[tagName]) {
                    wordDetails[tagName] += (', ' + xmlChildNode.getAttribute("value"))
                } else {
                    wordDetails[tagName] = xmlChildNode.getAttribute("value")
                }
                break
            case 'phonetic':
                let phoneticObj = {}
                phoneticObj['value'] = xmlChildNode.getAttribute("value")
                phoneticObj['soundFile'] = xmlChildNode.getAttribute("soundFile")
                wordDetails[tagName] = phoneticObj
                break
            case 'paradigm':
                const entryLang = xmlWord.getAttribute("lang")
                if (entryLang === 'sv') {   // only show inflections of Swedish word ( we may show SAOL results when the item is pressed)
                    wordDetails[tagName] = getCombinedStringFromOneTag(xmlChildNode, 'inflection')
                }
                break
            case 'example': // here we combine all example nodes to a new "examples"/"idioms"/"compound" field
            case 'idiom':
            case 'compound':
                let oneObj = {}
                oneObj['value'] = xmlChildNode.getAttribute("value")
                oneObj['translation'] = getCombinedStringFromOneTag(xmlChildNode, 'translation')
                let newField = tagName + 's'
                if (wordDetails[newField]) {
                    wordDetails[newField].push(oneObj)
                } else {
                    wordDetails[newField] = [oneObj]
                }
                break
            default:
                break
        }
    })

    return wordDetails
}

/**
 * extract values of all the child nodes of xmlParentNode which has tagName and combine the values into one string
 * @param xmlParentNode
 * @param tagName
 */
function getCombinedStringFromOneTag(xmlParentNode, tagName) {
    let childNodes = [...xmlParentNode.getElementsByTagName(tagName)]

    if (childNodes === null || childNodes.length === 0) {
        return ''
    }

    if (childNodes.length === 1) {
        return childNodes[0].getAttribute("value")
    }

    let combinedString = childNodes.reduce((acc, curr, index) => {
        let accString
        if (index === 1) {
            accString = acc.getAttribute("value")
        } else {
            accString = acc
        }
        return accString + ', ' + curr.getAttribute("value")
    })

    return combinedString
}

export default withStyles(styles)(DictionaryEntryList)