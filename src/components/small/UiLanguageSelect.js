import React from 'react'
import NativeSelect from '@material-ui/core/NativeSelect'
import PropTypes from "prop-types"

export default function UiLanguageSelect(props) {

    const handleChange = (event) => {
        let newLang = event.target.value
        props.onSelectLang(newLang)
    }

    return (
        <NativeSelect
            value={props.uiLanguage}
            onChange={handleChange}
        >
            <option value={'zh-Hans'}>中文</option>
            <option value={'en'}>English</option>
        </NativeSelect>
    )
}

UiLanguageSelect.propTypes = {
    uiLanguage: PropTypes.string.isRequired,
    onSelectLang: PropTypes.func.isRequired
}