import React from "react"
import PropTypes from "prop-types"
import {appbarHeigh, drawerWidth} from "../ResponsiveDrawer"
import {withStyles} from "@material-ui/core"
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'

const styles = theme => ({
    nested: {
        flex: 1,
        [theme.breakpoints.down('sm')]: {
            marginRight: 10,
            marginLeft: 10
        },
        [theme.breakpoints.up('sm')]: {
            marginRight: 100,
            marginLeft: drawerWidth + 100
        },
    },
});

class InformationPaper extends React.Component {
    static propTypes = {
        classes: PropTypes.object.isRequired,
        infoTitle: PropTypes.string.isRequired,
        infoText: PropTypes.string.isRequired
    }

    render() {
        return (
            <div className={this.props.classes.nested}>
                <Paper style={{ marginTop: appbarHeigh + 10}}>
                    <Typography variant="h5" component="h3">
                        {this.props.infoTitle}
                    </Typography>
                    <Typography component="p">
                        {this.props.infoText}
                    </Typography>

                </Paper>
            </div>
        )
    }
}

export default withStyles(styles)(InformationPaper)